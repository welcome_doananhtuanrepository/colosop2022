import React, { useState,useEffect } from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import Loading from "./Loading";
import {useDispatch} from "react-redux";
import {showItem} from "../Redux/productSlice"
import {BsSearch} from "react-icons/bs";
import {AiOutlineSearch} from "react-icons/ai"
import "../Style/products.css"
const Products = () => {
    const dispatch=useDispatch()
    const arrBtn=["All","Men","Women","Jewelery"]
    const [type, setType]=useState("All")
    const [data,setData]=useState([])
    const [filter,setFilter]=useState(data)
    const [isLoading,setIsloading]=useState(true)
    
    const handleClick=(item)=>{
        dispatch(showItem(item))
    }
    const fetch=async()=>{
        setIsloading(true)
        await axios({
            url:"https://632745af5731f3db995690a6.mockapi.io/product",
            method:"GET"
        })
        .then((res)=>{
            setIsloading(false)
            setData(res.data)
            setFilter(res.data)
        })
        .catch((error)=>{
            setIsloading(false)
            console.log(error)
        })
    }
    useEffect(()=>{
        fetch()
    },[])

    const filterProduct=(text)=>{
        setType(text)
        if(text==="All"){
            setFilter(data)
        }else{
            const newData=data.filter(x=>x.category===text)
            setFilter(newData)
        }
    }

    const filterSearchProduct = (input) => {
        const updateList = data.filter((x) => {
            return x.title.toLowerCase().includes(input);
        });
        setFilter(updateList);
    };

    const ShowProducts = () => {
        const [inputsearch, setInputsearch] = useState('');
        const handleKeyPress = (event) => {
            if (event.key === 'Enter') {
                filterSearchProduct(inputsearch);
            }
        };    
        
        const [currentPage,setCurrentPage]=useState(1)
        const [postPerPage,setPostPerPage]=useState(8)
        const [indexPage,setIndexPage]=useState(0)
        const indexOfLastPost=currentPage*postPerPage;
        const indexOfFirstPost=indexOfLastPost-postPerPage;
        const currentPost=filter.slice(indexOfFirstPost,indexOfLastPost)
        const pagesTotal=Math.ceil((filter.length)/postPerPage)
        console.log(pagesTotal)
        const pagesNumber=[]
        for(let i=1;i<pagesTotal+1;i++){
        pagesNumber.push(i)
        }
        const handleChangePage=(page)=>{
            setCurrentPage(page)
            setIndexPage(page-1)
        }
        return ( 
            <div className="showProducts">
                <div className="top_Products">
                    <div className="buttons">
                        {arrBtn.map((btn,index)=>(
                            <button 
                            style={btn===type?{backgroundColor:"violet"}:{backgroundColor:""}} 
                            key={index} 
                            className="buttonProducts" 
                            onClick={()=>filterProduct(btn)}>{btn}
                            </button>
                        ))}
                    </div>
                    <div className="searchProducts">
                        <AiOutlineSearch
                            size={"25px"}
                            style={{cursor:"pointer"}}
                            onClick={() => filterSearchProduct(inputsearch)}
                        />
                        <input
                            type="text"
                            value={inputsearch}
                            onChange={(e) => setInputsearch(e.target.value.toLowerCase())}
                            onKeyPress={handleKeyPress}
                            placeholder="Tên sản phẩm"
                        />
                    </div>
                </div>
                    <div className="row">
                        <div className="col">
                            <div className="product-grid">
                                {currentPost.map((item,index)=>(
                                    <div className="product-item men" key={index}>
                                        <div className="product discount product_filter">
                                            <div className="product_image">
                                                <img src={item.image[0]} alt=""/>
                                            </div>
                                            <div className="product_info">
                                                <h6 className="product_name">{item.title}</h6>
                                                <div className="product_price">${item.price}</div>
                                            </div>
                                        </div>
                                        <div onClick={()=>handleClick(item)} className="red_button add_to_cart_button">
                                            <Link to="/product" alt="">Chi tiết</Link>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                
                <ul className="numberPages">
                    {pagesNumber.map((page,index)=>(
                    <li  
                        onClick={()=>handleChangePage(page)}
                        key={index} 
                        className="numberPage" 
                        style={indexPage===index?{backgroundColor:"green"}:{backgroundColor:"aqua"}}>{page}
                    </li>
                    ))}
                </ul>
            </div>
         );
    }

    return ( 
        <div style={{overflow:"hidden"}}>
            <div>
                <h1 className="latestProduct">Tất cả sản phẩm</h1>
                <div className="listProducts">
                    {isLoading?<Loading />:<ShowProducts/>}
                </div>
            </div>
        </div>
     );
}
 
export default Products;