import React from "react";
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
const Loading = () => {
    return (
        <div className="container mx-auto">
            <div className="row">
                <div className="col-3">
                    <Skeleton height={300} />
                </div>
                <div className="col-3">
                    <Skeleton height={300} />
                </div>
                <div className="col-3">
                    <Skeleton height={300} />
                </div>
                <div className="col-3">
                    <Skeleton height={300} />
                </div>
            </div>
        </div>
        
    );
};
 
export default Loading;