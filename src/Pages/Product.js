
import React,{useState} from "react";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom"
import {useDispatch} from 'react-redux';
import {addToCart} from "../Redux/cartSlice"
import "../Style/Product.css"
const Product = () => {
    let product=useSelector(state=>state.product)
    const dispatch=useDispatch()
    const handleAddCart=()=>{
      let newProduct={...product,indexSize:indexSize}
      dispatch(addToCart(newProduct))
      alert("Sản phẩm đã được thêm vào giỏ hàng")
    }
    const [indexImg,setIndexImg]=useState(0)
    const [addClass,setAddClass]=useState(0)
    const [indexSize, setIndexSize]=useState(0)
    const handleChoose=(index)=>{
    setIndexImg(index)
    setAddClass(index)
    }
    const handleSize=(size)=>{
      setIndexSize(size)
    }
    if(product.id===""){
        return <h1 style={{textAlign:"center",marginTop:"60px"}}>Your cart is empty</h1>
    }
    return ( 
        <div className="Product">
        {
            <div className='details'>
                <div className='big-img'>
                  <img src={product.image[indexImg]} alt=""/>
                </div>
                <div className='box'>
                  <div className='row'>
                    <h2>{product.title}</h2>
                    <span style={{fontSize:"24px"}}>${product.price}</span>
                  </div>
                  <div className='colors'>
                    <div style={{display:"flex"}}>
                    {
                      product.size.map((item,index)=>(
                          <div className="btn" 
                          style={indexSize===index?{backgroundColor:"gray"}:{}}
                          onClick={()=>handleSize(index)}  
                          key={index}>
                          {item}
                          </div> 
                      ))
                    }
                    </div>
                    <p>{product.description}</p>
                    <div className='thumb'>
                      {
                        product.image.map((img,index)=>(
                          <img src={img} alt="" key={index}
                          onClick={()=>handleChoose(index)}
                          className={`${addClass===index?"active":""}`}
                          />
                        ))
                      }
                    </div>
                  </div>
                  <div style={{display:"flex"}}>
                    <button className='cart' onClick={handleAddCart}>Add to cart</button>
                    <Link to='/cart'><button style={{marginLeft:"10px"}} className='cart'>Go to cart</button></Link>
                  </div>
                  
                </div>
            </div>
   
        }
    </div>
     );
}
 
export default Product;