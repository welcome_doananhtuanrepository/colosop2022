import React from "react";
import "../../Style/blog.css"
import blog1 from "../../asset/images/blog_1.jpg"
import blog2 from "../../asset/images/blog_2.jpg"
import blog3 from "../../asset/images/blog_3.jpg"
const Blogg = () => {
    return ( 
        <div className="blogs">
		<div className="container">
			<div className="row">
				<div className="col text-center">
					<div className="section_title">
						<h2>Thông tin thời trang</h2>
					</div>
				</div>
			</div>
			<div className="row blogs_container">
				<div className="col-md-4 blog_item_col">
					<div className="blog_item">
						<div className="blog_background" style={{backgroundImage:`url(${blog1})`}}></div>
						<div className="blog_content d-flex flex-column align-items-center justify-content-center text-center">
							<h4 className="blog_title">Xu hướng thời trang mùa hè năm 2022</h4>
							<span className="blog_meta">by admin | dec 01, 2017</span>
							<a className="blog_more" href="/">Đọc thêm</a>
						</div>
					</div>
				</div>
				<div className="col-md-4 blog_item_col">
					<div className="blog_item">
						<div className="blog_background" style={{backgroundImage:`url(${blog2})`}}></div>
						<div className="blog_content d-flex flex-column align-items-center justify-content-center text-center">
							<h4 className="blog_title">Xu hướng thời trang mùa hè năm 2022</h4>
							<span className="blog_meta">by admin | dec 01, 2017</span>
							<a className="blog_more" href="/">Đọc thêm</a>
						</div>
					</div>
				</div>
				<div className="col-md-4 blog_item_col">
					<div className="blog_item">
						<div className="blog_background" style={{backgroundImage:`url(${blog3})`}}></div>
						<div className="blog_content d-flex flex-column align-items-center justify-content-center text-center">
							<h4 className="blog_title">Xu hướng thời trang mùa hè năm 2022</h4>
							<span className="blog_meta">by admin | dec 01, 2017</span>
							<a className="blog_more" href="/">Đọc thêm</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
     );
}
 
export default Blogg;