import React from "react";
import "../../Style/carousel.css"
import slider1 from "../../asset/images/slider_1.jpg";
import { Link } from "react-router-dom";
const Carousel = () => {
    return ( 
        <div className="main_slider" style={{backgroundImage:`url(${slider1})`}}>
			<div className="container fill_height">
				<div className="row align-items-center fill_height">
					<div className="col">
						<div className="main_slider_content">
							<h6 className="">Thời trang mùa xuân và hè</h6>
							<h1>Tiết kiệm hơn 30% khi mua tại đây</h1>
							<div className="red_button shop_now_button"><Link to='/products'>Mua hàng</Link></div>
						</div>
						{/* <img src={slider1} alt=""/> */}
					</div>
				</div>
			</div>
		</div>
     );
}
// 
export default Carousel;