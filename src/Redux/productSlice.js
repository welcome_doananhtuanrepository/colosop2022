import { createSlice } from '@reduxjs/toolkit'

export const productSlice = createSlice({
  name: 'product',
  initialState: {id:"",title:"",image:[],description:"",size:[],price:""},
  reducers: {
    showItem: (state, action) => {
        state.id=action.payload.id;
        state.title=action.payload.title;
        state.image=action.payload.image;
        state.description=action.payload.description;
        state.size=action.payload.size;
        state.price=action.payload.price;
      },
  },
})

// Action creators are generated for each case reducer function
export const {showItem} = productSlice.actions

export default productSlice.reducer